USE [master]
GO
/****** Object:  Database [bdMunicipios]    Script Date: 8/09/2020 3:44:14 a. m. ******/
CREATE DATABASE [bdMunicipios]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'bdMunicipios', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\bdMunicipios.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'bdMunicipios_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\bdMunicipios_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [bdMunicipios] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [bdMunicipios].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [bdMunicipios] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [bdMunicipios] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [bdMunicipios] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [bdMunicipios] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [bdMunicipios] SET ARITHABORT OFF 
GO
ALTER DATABASE [bdMunicipios] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [bdMunicipios] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [bdMunicipios] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [bdMunicipios] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [bdMunicipios] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [bdMunicipios] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [bdMunicipios] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [bdMunicipios] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [bdMunicipios] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [bdMunicipios] SET  DISABLE_BROKER 
GO
ALTER DATABASE [bdMunicipios] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [bdMunicipios] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [bdMunicipios] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [bdMunicipios] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [bdMunicipios] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [bdMunicipios] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [bdMunicipios] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [bdMunicipios] SET RECOVERY FULL 
GO
ALTER DATABASE [bdMunicipios] SET  MULTI_USER 
GO
ALTER DATABASE [bdMunicipios] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [bdMunicipios] SET DB_CHAINING OFF 
GO
ALTER DATABASE [bdMunicipios] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [bdMunicipios] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [bdMunicipios] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'bdMunicipios', N'ON'
GO
ALTER DATABASE [bdMunicipios] SET QUERY_STORE = OFF
GO
USE [bdMunicipios]
GO
/****** Object:  Table [dbo].[Municipio]    Script Date: 8/09/2020 3:44:15 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Municipio](
	[id_Municipio] [int] NOT NULL,
	[nombre_Municipio] [varchar](150) NOT NULL,
	[departamento_Municipio] [varchar](150) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_Municipio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Municipios_Cerca]    Script Date: 8/09/2020 3:44:15 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Municipios_Cerca](
	[id_MunicipioPrincipal] [int] NOT NULL,
	[id_MunicipioCercano] [int] NOT NULL
) ON [PRIMARY]
GO
INSERT [dbo].[Municipio] ([id_Municipio], [nombre_Municipio], [departamento_Municipio]) VALUES (1, N'Medellín', N'Antioquia')
GO
INSERT [dbo].[Municipio] ([id_Municipio], [nombre_Municipio], [departamento_Municipio]) VALUES (2, N'Bello', N'Antioquia')
GO
INSERT [dbo].[Municipio] ([id_Municipio], [nombre_Municipio], [departamento_Municipio]) VALUES (3, N'Envigado', N'Antioquia')
GO
INSERT [dbo].[Municipio] ([id_Municipio], [nombre_Municipio], [departamento_Municipio]) VALUES (4, N'Itagui', N'Antioquia')
GO
INSERT [dbo].[Municipio] ([id_Municipio], [nombre_Municipio], [departamento_Municipio]) VALUES (5, N'Sabaneta', N'Antioquia')
GO
INSERT [dbo].[Municipio] ([id_Municipio], [nombre_Municipio], [departamento_Municipio]) VALUES (6, N'Caldas', N'Antioquia')
GO
INSERT [dbo].[Municipio] ([id_Municipio], [nombre_Municipio], [departamento_Municipio]) VALUES (7, N'San Jeronimo', N'Antioquia')
GO
INSERT [dbo].[Municipio] ([id_Municipio], [nombre_Municipio], [departamento_Municipio]) VALUES (8, N'Sopetran', N'Antioquia')
GO
INSERT [dbo].[Municipio] ([id_Municipio], [nombre_Municipio], [departamento_Municipio]) VALUES (9, N'Barbosa', N'Antioquia')
GO
INSERT [dbo].[Municipios_Cerca] ([id_MunicipioPrincipal], [id_MunicipioCercano]) VALUES (1, 2)
GO
INSERT [dbo].[Municipios_Cerca] ([id_MunicipioPrincipal], [id_MunicipioCercano]) VALUES (1, 3)
GO
INSERT [dbo].[Municipios_Cerca] ([id_MunicipioPrincipal], [id_MunicipioCercano]) VALUES (2, 1)
GO
INSERT [dbo].[Municipios_Cerca] ([id_MunicipioPrincipal], [id_MunicipioCercano]) VALUES (3, 1)
GO
INSERT [dbo].[Municipios_Cerca] ([id_MunicipioPrincipal], [id_MunicipioCercano]) VALUES (3, 4)
GO
INSERT [dbo].[Municipios_Cerca] ([id_MunicipioPrincipal], [id_MunicipioCercano]) VALUES (4, 5)
GO
INSERT [dbo].[Municipios_Cerca] ([id_MunicipioPrincipal], [id_MunicipioCercano]) VALUES (5, 4)
GO
INSERT [dbo].[Municipios_Cerca] ([id_MunicipioPrincipal], [id_MunicipioCercano]) VALUES (1, 4)
GO
INSERT [dbo].[Municipios_Cerca] ([id_MunicipioPrincipal], [id_MunicipioCercano]) VALUES (3, 2)
GO
INSERT [dbo].[Municipios_Cerca] ([id_MunicipioPrincipal], [id_MunicipioCercano]) VALUES (3, 4)
GO
INSERT [dbo].[Municipios_Cerca] ([id_MunicipioPrincipal], [id_MunicipioCercano]) VALUES (3, 5)
GO
INSERT [dbo].[Municipios_Cerca] ([id_MunicipioPrincipal], [id_MunicipioCercano]) VALUES (4, 3)
GO
INSERT [dbo].[Municipios_Cerca] ([id_MunicipioPrincipal], [id_MunicipioCercano]) VALUES (4, 2)
GO
INSERT [dbo].[Municipios_Cerca] ([id_MunicipioPrincipal], [id_MunicipioCercano]) VALUES (2, 3)
GO
INSERT [dbo].[Municipios_Cerca] ([id_MunicipioPrincipal], [id_MunicipioCercano]) VALUES (2, 4)
GO
INSERT [dbo].[Municipios_Cerca] ([id_MunicipioPrincipal], [id_MunicipioCercano]) VALUES (2, 5)
GO
ALTER TABLE [dbo].[Municipios_Cerca]  WITH CHECK ADD FOREIGN KEY([id_MunicipioPrincipal])
REFERENCES [dbo].[Municipio] ([id_Municipio])
GO
ALTER TABLE [dbo].[Municipios_Cerca]  WITH CHECK ADD FOREIGN KEY([id_MunicipioCercano])
REFERENCES [dbo].[Municipio] ([id_Municipio])
GO
USE [master]
GO
ALTER DATABASE [bdMunicipios] SET  READ_WRITE 
GO
