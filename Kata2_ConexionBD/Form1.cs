﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;




namespace Kata2_ConexionBD
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ConexionBd.Conectar();
            MessageBox.Show("La conexion con la Base de datos ha sido exitosa");
            mostrarMunicipios(this.dataMunicipios, "select * from Municipio");
        }

        /*public DataTable mostrarMunicipios()
        {
            ConexionBd.Conectar();
            DataTable dt = new DataTable();
            string consulta = "select * from Municipio";
            SqlCommand cmd = new SqlCommand(consulta, ConexionBd.Conectar());

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            return dt;

        }*/

        public void mostrarMunicipios(DataGridView dg, string consulta)
        {
            ConexionBd.Conectar();
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(consulta,ConexionBd.Conectar());

            da.Fill(ds, "Municipio");
            dg.DataSource = ds;
            dg.DataMember = "Municipio";


        }


        public void limpiar()
        {
            txtId.Text = "";
            txtNombre.Text = "";
            txtDepartamente.Text = "";
        }

        private void btnInsertar_Click(object sender, EventArgs e)
        {
            ConexionBd.Conectar();
            string insertar = "insert into Municipio (id_Municipio,nombre_Municipio, departamento_Municipio) values (@id,@nombre,@departamento)";
            SqlCommand cmd1 = new SqlCommand(insertar, ConexionBd.Conectar());
            cmd1.Parameters.AddWithValue("@id", txtId.Text);
            cmd1.Parameters.AddWithValue("@nombre", txtNombre.Text);
            cmd1.Parameters.AddWithValue("@departamento", txtDepartamente.Text);

            cmd1.ExecuteNonQuery();

            MessageBox.Show("Municipio agregado.");
            mostrarMunicipios(this.dataMunicipios, "select * from Municipio");
            limpiar();
        }

        private void txtBuscar_KeyUp(object sender, KeyEventArgs e)
        {
            mostrarMunicipios(this.dataMunicipios, "select * from Municipio where Municipio.nombre_Municipio like '" + txtBuscar.Text + "%'; ");
        }
    }
}
